<?php

use app\Controllers\ArticleController;
use app\Controllers\UserController;
use SamIndustry\Framework\Route;

Route::add('/', [UserController::class, 'login']);
Route::add('/user/valid', [UserController::class, 'validUser']);
Route::add('/registration', [UserController::class, 'registration']);
Route::add('/user/create', [UserController::class, 'createUser']);
Route::add('/user/logout', [UserController::class, 'logoutUser']);


Route::add('/home', [ArticleController::class, 'home']);
Route::add('/articles/output', [ArticleController::class, 'articlesOutput']);
Route::add('/article/save', [ArticleController::class, 'saveArticles']);
