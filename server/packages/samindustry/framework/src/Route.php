<?php

namespace SamIndustry\Framework;

use http\Encoding\Stream\Inflate;

class Route
{
    protected static array $routes = [];
    protected static array $route = [];

    /**
     * @param string $url
     * @param array $route
     * @return void
     */
    public static function add(string $url, array $route = []): void
    {
        self::$routes[$url] = $route;
    }

    /**
     * @param string $url
     * @return void
     */
    public static function dispatch(string $url): void
    {
        if (self::matchRoute($url)) {

            $controller = self::$route[0];
            if (class_exists($controller)) {

                $contrObj = new $controller(self::$route);
                $action = self::$route[1];
                 if(method_exists($contrObj,$action)){
                     if(isset(self::$route[2])){
                        $params = self::$route[2];
                         $contrObj->$action($params);
                     }else{
                         $contrObj->$action();
                     }

                 }else{
                     echo 'Method ' . $action . ' not found:(';
                 }
            } else {
                echo 'Controller ' . $controller . ' not found :(';
            }

        } else {

            http_response_code(404);
            include '404.html';

        }
    }

    /**
     * @param string $url
     * @return bool
     */
    public static function matchRoute(string $url): bool
    {
        foreach (self::$routes as $pattern => $route) {
            if ($pattern == $url) {
                self::$route = $route;
                return true;
            }
        }
        foreach(self::$routes as $pattern => $route){
            if(preg_match("~$pattern~", $url) && $pattern !== '/'){
                $route[1] = preg_replace("~$pattern~", $route[1], $url);
                $segments = explode('/', $route[1]);
                $route[1] = array_shift($segments);
                $route[2] = $segments[0];
                self::$route = $route;
                return true;
            }
        }
        return false;
    }
}
