<?php

namespace SamIndustry\Framework\Models;

use SamIndustry\Framework\DB;

/**
 * Class Model
 * @package SamIndustry\Framework\Models
 */
abstract class Model
{
    protected DB $pdo;
    protected string $table;

    /**
     * Model constructor.
     */
    public function __construct()
    {
        $this->pdo = DB::instance();
    }

    /**
     * @param string $column
     * @param string $operator
     * @param mixed $data
     * @param mixed $columnSearch
     * @return array
     */
    public function whereOne(string $column, string $operator, mixed $data, mixed $columnSearch = '*'):array
    {
        $result = $this->where($column, $operator, $data, $columnSearch);
        if(empty($result)){
            return [];
        }else{
            return $result[0];
        }
    }

    /**
     * @param string $whereColumn
     * @param string $operator
     * @param mixed $data
     * @param mixed $columnSearch
     * @return array
     */
    public function where(string $whereColumn, string $operator, mixed $data, mixed $columnSearch = '*'):array
    {
        if(!is_string($columnSearch)){
            $columnSearch = implode(",", $columnSearch);
        }
        $sql = "SELECT $columnSearch FROM $this->table WHERE $whereColumn $operator '$data'";
        $result = $this->pdo->query($sql);

        return $result;
    }

    /**
     * @param string $table1
     * @param string $table2
     * @param string $columnTable1
     * @param string $columnTable2
     * @param string $whereColumn
     * @param mixed $whereData
     * @param mixed $columnSearch
     * @return array
     */
    public function joinWhere(string $table1, string $table2, string $columnTable1, string $columnTable2, string $whereColumn, mixed $whereData, mixed $columnSearch = '*'):array
    {

        if(!is_string($columnSearch)){
            $columnSearch = implode(",", $columnSearch);
        }
        $sql = "SELECT $columnSearch FROM $table1 JOIN $table2 ON $table1.$columnTable1 = $table2.$columnTable2 WHERE $whereColumn = '$whereData'";
        $result = $this->pdo->query($sql);
        return $result;
    }

    /**
     * @param array $array
     * @param array $additionalColumns
     * @return array
     */
    public function insert(array $array, $additionalColumns = []): array
    {
        $array = array_map(function ($val) {
            return htmlspecialchars($val);
        }, $array);

        if(!empty($additionalColumns)){
            $array = array_merge($array, $additionalColumns);
        }

        $keysString = implode(',', array_keys($array));
        $valuesString = implode('\',\'', array_values($array));

        $sql = "INSERT INTO $this->table ($keysString) VALUES ('$valuesString')";

        return $this->pdo->query($sql);
    }

}
