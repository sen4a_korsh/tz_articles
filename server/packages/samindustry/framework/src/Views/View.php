<?php
namespace SamIndustry\Framework\Views;

/**
 * Class View
 * @package SamIndustry\Framework\Views
 */
class View
{
    /**
     * @var array
     */
    public array $route = [];

    /**
     * @var string
     */
    public string $viewpath;

    /**
     * View constructor.
     * @param array $route
     * @param string $viewpath
     */
    public function __construct(array $route, string $viewpath = '')
    {
        $this->route = $route;
        $this->viewpath = $viewpath;
    }

    /**
     * @param array $data
     */
    public function render(array $data): void
    {

        if(is_array($data)){
            extract($data);
        }
        $fileView = ROOT . '/app/Views/' . $this->viewpath . '.view.php';

        ob_start();
        if(is_file($fileView)){
            require  $fileView;
        }else{
            echo '<h1>File not found! '. $fileView .'</h1>';
        }
        $content = ob_get_clean();

        $fileLayout = ROOT . '/app/Views/layouts/default.layout.php';
        if(is_file($fileLayout)){
            require $fileLayout;
        }else{
            echo 'layouts not found';
        }
    }

    /**
     * @param array $data
     */
    public function renderLayouts(array $data):void
    {
        if(is_array($data)){
            extract($data);
        }
        $fileLayouts = ROOT . '/app/Views/' . $this->viewpath . '.layout.php';

        if(is_file($fileLayouts)){
            require  $fileLayouts;
        }else{
            echo '<h1>File not found! '. $fileLayouts .'</h1>';
        }

    }
}
