<?php

define('URI', trim($_SERVER['REQUEST_URI'], ''));
define('ROOT', dirname(__DIR__));

require_once '../vendor/autoload.php';
require_once  (ROOT . '/routes/web.php');

use SamIndustry\Framework\Route;
use Symfony\Component\Dotenv\Dotenv;

session_start();

$dotenv = new Dotenv();
$dotenv->load(ROOT . '/.env');

Route::dispatch(URI);
