$(document).ready(function (e) {
    let arrArticles = []

    $(".btn-position").click(function (e) {
        e.preventDefault()

        let id_articles = $(".btn-position").attr("href").split('/').slice(-1);
        let idBtn = $(this).attr("id");

        if (id_articles <= 10) {
            $(".btn-position").attr("href", "http://jsonplaceholder.typicode.com/posts/" + ++id_articles)
            $.ajax({
                url: $(this).attr('href'),
                type: 'GET',
                success(result) {
                    result['button'] = idBtn;
                    arrArticles.push(result);
                    console.log(arrArticles);

                    $.ajax({
                        url: '/articles/output',
                        type: 'POST',
                        data: {
                            arrArticles: arrArticles,
                        },
                        success(resultArticles) {
                            $('#articles').html(resultArticles);
                        }
                    })
                }
            })
        }
    })
    $("#form-articles").submit(function (e) {
        e.preventDefault()
        console.log('form')
        console.log(arrArticles)
        if (!$.isEmptyObject(arrArticles)){
            $.ajax({
                url: $(this).attr('action'),
                type: 'POST',
                data: {
                    arrArticles: arrArticles,
                },
                success(resultArticles) {
                    $('#articles').html(resultArticles);
                    arrArticles = []
                }
            })
        }
    })

})
