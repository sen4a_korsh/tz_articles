$(document).ready(function () {

    $("#form_regist").submit(function (e) {
        e.preventDefault();

        $.ajax({
            url: $(this).attr('action'),
            type: $(this).attr('method'),
            data: new FormData(this),
            contentType: false,
            cache: false,
            processData: false,
            success (result){
                if(!result){
                    document.location.href = '/home';
                }else {
                    $('#error').html(result);
                }
            }
        })
    })
})
