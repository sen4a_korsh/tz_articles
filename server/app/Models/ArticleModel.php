<?php


namespace app\Models;


use SamIndustry\Framework\Models\Model;

/**
 * Class ArticleModel
 * @package app\Models
 */
class ArticleModel extends Model
{
    protected string $table = 'articles';

    private static array $message = [
        'successSave' => '<h2>Записи успешно сохранены!</h2>',
    ];

    /**
     * @param array $articles
     * @return string
     */
    public static function saveArticles(array $articles): string
    {
        $articleModel = new self;
        $sql = '';
        foreach ($articles as $value){

            $title = $value['title'];
            $body = $value['body'];
            $button = $value['button'];
            $id_user = $_SESSION['user']['id_user'];

            $sql .= "INSERT INTO `articles` (`title`, `body`, `button`, `id_user`) VALUES ( '$title', '$body', '$button', '$id_user'); ";
        }
        $articleModel->pdo->query($sql);

        return self::$message['successSave'];
    }
}
