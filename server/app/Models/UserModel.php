<?php

namespace app\Models;

use SamIndustry\Framework\Models\Model;

/**
 * Class UserModel
 * @package app\Models
 */
class UserModel extends Model
{

    protected string $table = 'users';

    private static array $errors = [
        'errorEmailBusy' => '<h5>Эта почта занята!</h5>',
        'errorEmailIncorrect' => '<h5>Почта введена некорректно!</h5>',
        'errorShortPassword' => '<h5>Пароль короче 6 символов</h5>',
        'errorPasswordSpecChar' => '<h5>Пароль должен содержать символ "!"</h5>',
        'validError' => '<h5>Неверный email или пароль</h5>',
        'checkInputsError' => '<h5>Заполните все поля!</h5>',
    ];

    /**
     * @param array $formData
     * @return string
     */
    public static function createUser(array $formData): string
    {
        $userModel = new self;

        $checkInputs = $userModel::checkInputs($formData);

        if (!empty($checkInputs)) {
            return $checkInputs;
        }

        $checkEmail = $userModel::checkEmail($formData['email']);
        if (!empty($checkEmail)) {
            return $checkEmail;
        }

        $checkPassword = $userModel::checkPassword($formData['password']);
        if (!empty($checkPassword)) {
            return $checkPassword;
        }

        $userEmail = $userModel->whereOne('email', '=', $formData['email'], ['email']);

        if (empty($userEmail)) {
            $formData['password'] = password_hash($formData['password'], PASSWORD_BCRYPT);

            $userModel->insert($formData);

            $user = $userModel::getUserData($formData);
            $_SESSION['user'] = $user;

            return '';
        } else {
            return self::$errors['errorEmailBusy'];
        }
    }

    /**
     * @param array $formData
     * @return string
     */
    public static function validUser(array $formData): string
    {
        $userModel = new self;

        $checkInputs = $userModel::checkInputs($formData);
        if (!empty($checkInputs)) {
            return $checkInputs;
        }

        $userPassword = $userModel->whereOne('email', '=', $formData['email'], 'password');

        if (!empty($userPassword)) {
            if (password_verify($formData['password'], $userPassword['password'])) {
                $user = $userModel::getUserData($formData);
                $_SESSION['user'] = $user;
                return '';
            }
        }
        return self::$errors['validError'];
    }

    /**
     * @param array $formData
     * @return string
     */
    private static function checkInputs(array $formData): string
    {
        foreach ($formData as $value) {
            if (empty($value)) {
                return self::$errors['checkInputsError'];
            }
        }
        return '';
    }

    /**
     * @param string $email
     * @return string
     */
    private static function checkEmail(string $email): string
    {
        $patternEmail = '/[a-zA-Z]{3}@[a-zA-Z]{3}/';

        if(!preg_match($patternEmail, $email)){
            return self::$errors['errorEmailIncorrect'];
        }
        return '';
    }

    /**
     * @param array $formData
     * @return array
     */
    private static function getUserData(array $formData): array
    {
        $userModel = new self;
        return $userModel->joinWhere(
            'users',
            'positions',
            'id_position',
            'id_position',
            'users.email',
            $formData['email'],
            'users.id_user, users.first_name, users.last_name, users.email, positions.name_position'
        )[0];
    }

    /**
     * @param string $password
     * @return string
     */
    private static function checkPassword(string $password): string
    {
        if (strlen($password) < 6) {
            return self::$errors['errorShortPassword'];
        }
        if (empty(preg_match('/!/', $password))) {
            return self::$errors['errorPasswordSpecChar'];
        }
        return '';
    }

}
