<?php $_SESSION['user'] ?? header('Location: '. $_ENV['APP_URL'] . '/');
$position = $_SESSION['user']['name_position'];
?>
<div class="container feed">
    <div class="navbar account-info">
        <div class="user-data">
            <div><h2></h2></div>
            <div><h2>Спасибо, что вошли, <?=$_SESSION['user']['first_name']. ' ' .$_SESSION['user']['last_name']?> !!</h2></div>
            <div><?=$_SESSION['user']['email']?></div>
            <div><h5><?=$_SESSION['user']['name_position']?></h5></div>
        </div>
    </div>
    <div class="feed-article">
        <form action="/article/save" id="form-articles" method="post">
            <div class="navbar position">
                <div class="position-item">
                    <h3>BOSS</h3>
                    <a class="btn btn-danger btn-position <?=$position !='Boss' ? 'inactive-btn' : ''?>" id="Кнопка Boss" href="">Кнопка Boss</a>
                </div>
                <div class="position-item">
                    <h3>Manager</h3>
                    <a class="btn btn-danger btn-position <?=($position !='Manager' && $position !='Boss')  ? 'inactive-btn' : ''?>"
                       id="Кнопка Manager" href="http://jsonplaceholder.typicode.com/posts/1">Кнопка Manager</a>
                </div>
                <div class="position-item">
                    <h3>Performer</h3>
                    <a class="btn btn-danger btn-position" id="Кнопка Performer" href="http://jsonplaceholder.typicode.com/posts/1">Кнопка Performer</a>
                </div>
            </div>
            <div class="container">
                <div class="row row-cols-3 articles" id="articles">
                    <div class="messAddArticle"><h4>Добавьте статью</h4></div>
                </div>
            </div>
            <div class="btn-block">
                <button class="btn btn-primary btn_save" type="submit">Сохранить</button>
            </div>
        </form>
    </div>
</div>
<script src="../js/articles.js"></script>
