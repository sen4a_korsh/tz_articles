<?php foreach ($articles as $value):?>
<div class="col article-block">
    <h3><?=$value['title']?></h3>
    <div class="article-body">
        <p><?=$value['body']?></p>
    </div>
</div>
<?php endforeach;
