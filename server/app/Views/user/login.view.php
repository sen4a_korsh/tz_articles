<?php !isset($_SESSION['user']) ?: header('Location: '. $_ENV['APP_URL'] . '/home')?>
<form class="container form-regist" id="form_login" name="form_login" method="POST" action="/user/valid">
    <div class="title-regist">
        <h1>Вход</h1>
    </div>
    <div class="mb-3">
        <label class="form-label">Email:</label>
        <input type="email" maxlength="100" class="form-control" name="email" placeholder="Введите ваш Email">
    </div>
    <div class="mb-3">
        <label class="form-label">Пароль:</label>
        <input type="password" maxlength="45" class="form-control" name="password" placeholder="Введите ваш пароль">
    </div>
    <div class="mb-3 error" id="error">
    </div>
    <div class="submit-regist">
        <button type="submit" class="btn btn-primary">Войти</button>
    </div>
</form>
<script src="../js/login.js"></script>
