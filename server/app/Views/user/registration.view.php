<?php !isset($_SESSION['user']) ?: header('Location: '. $_ENV['APP_URL'] . '/home')?>
<form class="container form-regist" id="form_regist" name="form_registr" method="POST" action="/user/create">
        <div class="title-regist">
            <h1>Регистрация</h1>
        </div>
        <div class="mb-3">
            <label class="form-label">Имя:</label>
            <input type="text" maxlength="30" class="form-control" id="first_name" name="first_name" placeholder="Введите ваше имя">
        </div>
        <div class="mb-3">
            <label class="form-label">Фамилия:</label>
            <input type="text" maxlength="70" class="form-control" id="last_name" name="last_name" placeholder="Введите вашу фамилию">
        </div>
        <div class="mb-3">
            <label class="form-label">Email:</label>
            <input type="email" maxlength="60" class="form-control" id="email" name="email" placeholder="Введите ваш Email">
        </div>
        <div class="mb-3">
            <label class="form-label">Должность:</label>
            <select class="form-select" name="id_position">
                <option value="" selected>Выбирете вашу должность</option>
                <option value="1">Босс</option>
                <option value="2">Менеджер</option>
                <option value="3" >Исполнитель</option>
            </select>
        </div>
        <div class="mb-3">
            <label class="form-label">Пароль:</label>
            <input type="password" maxlength="45" class="form-control" id="password" name="password" placeholder="Введите пароль">
        </div>
        <div class="mb-3 error" id="error">
        </div>
        <div class="submit-regist">
            <button type="submit" class="btn btn-primary">Зарегестрироватся</button>
        </div>
</form>
<script src="../js/registration.js"></script>
