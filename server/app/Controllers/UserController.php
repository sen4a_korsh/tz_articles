<?php

namespace app\Controllers;

use app\Models\UserModel;
use SamIndustry\Framework\Controllers\Controller;

/**
 * Class UserController
 * @package app\Controllers
 */
class UserController extends Controller
{

    public function login(): void
    {
        $title = 'Login';
        $nav = 'login';
        $this->view('user/login', ['title' => $title, 'nav' => $nav]);
    }

    public function validUser(): void
    {
        $formData = $_POST;
        echo UserModel::validUser($formData);
    }

    public function registration(): void
    {
        $title = 'Registration';
        $nav = 'registration';
        $this->view('user/registration', ['title' => $title, 'nav' => $nav]);
    }

    public function createUser(): void
    {
        $formData = $_POST;
        echo UserModel::createUser($formData);
    }


    public function logoutUser(): void
    {
        unset($_SESSION['user']);
        header('Location: '. $_ENV['APP_URL'] . '/');
    }
}
