<?php


namespace app\Controllers;

use app\Models\ArticleModel;
use SamIndustry\Framework\Controllers\Controller;

/**
 * Class ArticleController
 * @package app\Controllers
 */
class ArticleController extends Controller
{

    public function home(): void
    {
        $title = 'Home';
        $nav = 'main';
        $this->view('articles/home', ['title' => $title, 'nav' => $nav]);
    }

    public function articlesOutput(): void
    {
        $articles = $_POST['arrArticles'];
        $this->layout('layouts/articles', ['articles' => $articles]);
    }

    public function saveArticles(): void
    {
        $articles = $_POST['arrArticles'];
        echo ArticleModel::saveArticles($articles);
    }

}

